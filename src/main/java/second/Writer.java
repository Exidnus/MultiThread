package second;

import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;

public class Writer implements Runnable {

    private volatile boolean isRunning = true; 
    private FileWriter writer; //final

    ArrayBlockingQueue<String> queueForWrite; //private final

    public Writer(String path, ArrayBlockingQueue<String> queueForWrite) throws IOException {
        writer = new FileWriter(path);
        this.queueForWrite = queueForWrite;
    }

    //закрытие через метов close() 
    @Override
    public void run() {
        // boolean b = true;
        while (isRunning){
            Strign next = queueForWrite.poll(5, TimeUnit.SECONDS); 
            if (next != null) {
                writer.write(next + "\n"); // попробуй в Windows это запустить, кстати. 
            }
            // if (queueForWrite.peek() != null) {
            //     if (queueForWrite.peek().equals("@end")) {
            //         break;
            //     }
            //     try {
            //         writer.write(queueForWrite.poll() + "\n");
            //     } catch (IOException e) {
            //         e.printStackTrace();
            //     }
            // }
        }
        try {
            writer.close(); // try-with-resources
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
