package second;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Filter implements Runnable {

    AtomicReference<String> line = new AtomicReference<>(); //private final

    private ArrayBlockingQueue<String> queueForRear; // final
    private ArrayBlockingQueue<String> queueForWrite; // final
    private final String regex;

    Flag flag;

    public Filter(ArrayBlockingQueue<String> queueForRear, ArrayBlockingQueue<String> queueForWrite, String regex, Flag flag) {
        this.queueForRear = queueForRear;
        this.queueForWrite = queueForWrite;
        this.regex = regex;
        this.flag = flag;
    }

    private boolean checkWithRegExp(String string) {
        if (string == null)
            return false;
        Pattern p = Pattern.compile(regex); //паттерн лучше в конструкторе скомпиллировать, это относительно дорогая операция
        Matcher m = p.matcher(string);
        return m.matches() || string.equals("@end");
    }

    @Override
    public void run() {
        if (queueForRear.peek() != null) { //peek + poll - не атомарная операция. Один поток делает пик, другой в это время пол, очередь пустая, и первый 
        //поток на поле получает null. 
            line.set(queueForRear.poll());
            if (line.get().equals("@end"))
                flag.setFlag(false);
            if (checkWithRegExp(line.get())) {
                queueForWrite.add(line.get());
            }
        }
    }
}
