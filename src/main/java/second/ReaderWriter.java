package second;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReaderWriter {

//зачем? 
    Flag flag = new Flag(); //private final

    String startFilePath; //private final
    String resultFilePath; //private final 

    private int poolSize; //final
    private Boolean save; //final зачем boxing? 
    private final String regex; //final

    private ArrayBlockingQueue<String> queueForRead = new ArrayBlockingQueue<>(200); //final
    private ArrayBlockingQueue<String> queueForWrite = new ArrayBlockingQueue<>(200); //final

    private ExecutorService service;

    public ReaderWriter(String startFilePath, String resultFilePath, int poolSize, Boolean save, String regex) throws IOException {

        this.startFilePath = startFilePath;
        this.resultFilePath = resultFilePath;

        service = Executors.newCachedThreadPool();

        this.poolSize = poolSize;
        this.save = save;
        this.regex = regex;

    }

    public void filter() throws IOException, ExecutionException, InterruptedException {

        service.execute(new Reader(startFilePath, queueForRead));
        ArrayList<Runnable> tasks = new ArrayList<>();
        service.execute(new Writer(resultFilePath, queueForWrite));

        while (flag.isFlag()){
            for (int i = 0; i < poolSize; i++) {

                tasks.add(new Filter(queueForRead, queueForWrite, regex, flag));
            }
            for (int i = 0; i < tasks.size(); i++) {
                service.execute(tasks.get(i));
            }
            tasks.clear();
        }
    }

    public void close(){
        service.shutdown();
    }

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        ReaderWriter readerWriter = new ReaderWriter("/Users/macbook/IdeaProjects/MultiThread/src/main/resources/start.txt",
                "/Users/macbook/IdeaProjects/MultiThread/src/main/resources/result.txt", 5, true, "Д.*");
        long t1 = System.nanoTime();
        readerWriter.filter();
        long t2 = System.nanoTime();
        System.out.println(t2 - t1);
        readerWriter.close();
    }


}
