package second;

import java.io.*;
import java.util.concurrent.ArrayBlockingQueue;

public class Reader implements Runnable {

    private BufferedReader reader; //final
 
    ArrayBlockingQueue<String> queueForRead; //private final

    public Reader(String path, ArrayBlockingQueue<String> queueForRead) throws FileNotFoundException {
        reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(path)));
        this.queueForRead = queueForRead;
    }

    //закрытие через метод close(), обсуждали же
    @Override
    public void run() {
        try {
            while (true) {
                String line = reader.readLine();
                if (line != null) {
                    queueForRead.add(line);
                } else {
                    queueForRead.add("@end");
                    reader.close();
                    break;
                }

            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
