package first;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Downloader{
    private final ExecutorService service;

    // final int poolSize;

    public Downloader(int poolSize) {
        // this.poolSize = poolSize;
        service = Executors.newFixedThreadPool(poolSize);
    }

    public List<String> downloadContent(List<String> urls) {
        List<String> contents = new ArrayList<>(urls.size());
        List<Future<String>> tasks = new ArrayList<>(urls.size());
        try {
            // Как-то очень сложно выглядит. 
            // for (int i = 0; i < urls.size();) {
            //     for (int j = 0; j < poolSize; j++) {
            //         if (i > 14)
            //             break;
            //         tasks.add(new FutureTask<>(new DownloadContent(urls.get(i))));
            //         service.submit(tasks.get(j));
            //         i++;
            //     }
            //     for (int j = 0; j < tasks.size(); j++) {
            //         contents.add(tasks.get(j).get());
            //     }
            //     tasks.clear();
            // }

            for (String url : urls) {
                tasks.add(service.submit(new DownloadContent(url))); 
            }

            for (Future<String> future : tasks) {
                contents.add(future.get(10, TimeUnit.SECONDS)); //ждем с таймаутом, чтобы не подвиснуть при каких-то проблемах + в идеале перехватываем эксепшен 
                //при истечении таймаута
            }
        }catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return contents;
    }

    public void close() {
        service.shutdown(); //shutdown() не дожидается, пока задачи выполнялся, только запрещает сабмиттить новые задачи в ExecutorService
        service.awaitTermination(10, TimeUnit.SECONDS); //ждем, пока все задачи выполнятся
    }

    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<>();
        list.add("https://mail.ru");
        list.add("https://habr.com/company/luxoft/blog/157273/");
        list.add("http://java-online.ru/concurrent-executor.xhtml");
        list.add("https://javadevblog.com/java-callable-kratkoe-opisanie-i-primer-ispol-zovaniya.html");
        list.add("https://yandex.ru/maps/2/saint-petersburg/?ll=30.315868%2C59.939095&z=11");
        list.add("https://mail.ru");
        list.add("https://habr.com/company/luxoft/blog/157273/");
        list.add("http://java-online.ru/concurrent-executor.xhtml");
        list.add("https://javadevblog.com/java-callable-kratkoe-opisanie-i-primer-ispol-zovaniya.html");
        list.add("https://yandex.ru/maps/2/saint-petersburg/?ll=30.315868%2C59.939095&z=11");
        list.add("https://mail.ru");
        list.add("https://habr.com/company/luxoft/blog/157273/");
        list.add("http://java-online.ru/concurrent-executor.xhtml");
        list.add("https://javadevblog.com/java-callable-kratkoe-opisanie-i-primer-ispol-zovaniya.html");
        list.add("https://yandex.ru/maps/2/saint-petersburg/?ll=30.315868%2C59.939095&z=11");

        Downloader downloader = new Downloader(4);
        System.out.println(downloader.downloadContent(list));
        downloader.close();

    }
}
